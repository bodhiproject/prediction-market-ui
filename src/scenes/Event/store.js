import { observable, runInAction, action, computed, reaction, toJS } from 'mobx';
import { sum, isUndefined } from 'lodash';
import axios from 'axios';
import numbro from 'numbro';
import NP from 'number-precision';
import { EventWarningType, EVENT_STATUS, TransactionStatus, Routes, EVENT_TYPE } from 'constants';
import { toFixed, satoshiToDecimal, decimalToSatoshi, eventTypeToAppType } from '../../helpers/utility';
import { API } from '../../network/routes';
import {
  events,
  totalResultBets,
  withdraws,
  eventLeaderboardEntries,
} from '../../network/graphql/queries';
import { maxTransactionFee } from '../../config/app';

const { PRE_BETTING, BETTING, PRE_RESULT_SETTING, ORACLE_RESULT_SETTING, OPEN_RESULT_SETTING, ARBITRATION, WITHDRAWING } = EVENT_STATUS;
const INIT = {
  loading: true,
  event: undefined,
  address: '',
  escrowAmount: 0,
  totalBets: [],
  betterBets: [],
  totalVotes: [],
  betterVotes: [],
  totalInvestment: 0,
  returnRate: 0,
  profitOrLoss: 0,
  pendingWithdraw: [],
  nbotWinnings: 0, // number
  amount: '',
  selectedOptionIdx: -1,
  buttonDisabled: false,
  warningType: '',
  eventWarningMessageId: '',
  error: {
    amount: '',
    address: '',
  },
  didWithdraw: undefined, // boolean
  betterReturn: 0,
  voterReturn: 0,
  creatorReturn: 0,
};

export default class EventStore {
  @observable loading = INIT.loading
  @observable event = INIT.event
  @observable address = INIT.address
  @observable totalBets = INIT.totalBets
  @observable betterBets = INIT.betterBets
  @observable totalVotes = INIT.totalVotes
  @observable betterVotes = INIT.betterVotes
  @observable nbotWinnings = INIT.nbotWinnings
  @observable amount = INIT.amount // Input amount to bet, vote, etc
  @observable selectedOptionIdx = INIT.selectedOptionIdx // Current result index selected
  @observable buttonDisabled = INIT.buttonDisabled
  @observable warningType = INIT.warningType
  @observable eventWarningMessageId = INIT.eventWarningMessageId
  @observable error = INIT.error
  @observable escrowAmount = INIT.escrowAmount
  @observable didWithdraw = INIT.didWithdraw
  @observable pendingWithdraw = INIT.pendingWithdraw
  @observable totalInvestment = INIT.totalInvestment
  @observable returnRate = INIT.returnRate
  @observable profitOrLoss = INIT.profitOrLoss
  @observable betterReturn = INIT.betterReturn
  @observable voterReturn = INIT.voterReturn
  @observable creatorReturn = INIT.creatorReturn

  @computed get eventName() {
    return this.event && this.event.name;
  }

  @computed get isBetting() {
    return this.event && [PRE_BETTING, BETTING].includes(this.event.status);
  }

  @computed get isResultSetting() {
    return this.event
      && [PRE_RESULT_SETTING, ORACLE_RESULT_SETTING, OPEN_RESULT_SETTING].includes(this.event.status);
  }

  @computed get isArbitration() {
    return this.event && this.event.status === ARBITRATION;
  }

  @computed get isWithdrawing() {
    return this.event && this.event.status === WITHDRAWING;
  }

  @computed get maxLeaderBoardSteps() {
    return this.event.status === WITHDRAWING ? 2 : 1;
  }

  @computed get selectedOption() {
    return (this.event.results && this.event.results[this.selectedOptionIdx]) || {};
  }

  @computed get withdrawableAddress() {
    const { currentAddress } = this.app.wallet;
    const resultIndex = this.event && this.event.currentResultIndex;
    if (this.betterBets.length === 0 ||
      this.betterVotes.length === 0 ||
      this.totalBets.length === 0 ||
      this.totalVotes.length === 0) return {};
    const betterBetsSum = sum(this.betterBets);
    const betterVotesSum = sum(this.betterVotes);
    switch (this.event.eventType) {
      case EVENT_TYPE.REWARD_EVENT:
      case EVENT_TYPE.AB_EVENT: {
        let totalInvestment = betterBetsSum + betterVotesSum;
        if (this.event.ownerAddress === currentAddress) totalInvestment += this.escrowAmount;
        return {
          address: currentAddress,
          creator: this.event.ownerAddress,
          creatorReturn: this.creatorReturn,
          yourTotalBets: betterBetsSum,
          yourTotalBetsReturn: this.betterReturn,
          yourTotalBetsReturnRate: (this.betterReturn / betterBetsSum) * 100,
          yourTotalVotes: betterVotesSum,
          yourTotalVotesReturn: this.voterReturn,
          yourTotalVotesReturnRate: (this.voterReturn / betterVotesSum) * 100,
          yourTotalReturn: this.betterReturn + this.voterReturn
            + this.creatorReturn,
          yourWinningInvestment: this.betterBets[resultIndex]
            + this.betterVotes[resultIndex],
          yourTotalReturnRate: ((this.betterReturn + this.voterReturn
            + this.creatorReturn) / totalInvestment) * 100,
        };
      }
      case EVENT_TYPE.MR_EVENT: {
        const profitCut = this.event && this.event.arbitrationRewardPercentage;
        const totalBetsSum = sum(this.totalBets);
        const totalVotesSum = sum(this.totalVotes);
        let totalWinningBets;
        let totalLosingBets;
        let arbitrationShare;
        let betShare;
        let betterBetsReturn;
        if (resultIndex === 0) {
          betterBetsReturn = betterBetsSum;
          arbitrationShare = 0;
        } else {
          totalWinningBets = this.totalBets[resultIndex];
          totalLosingBets = totalBetsSum - totalWinningBets;
          arbitrationShare = (totalLosingBets * profitCut) / 100;
          betShare = totalLosingBets - arbitrationShare;
          betterBetsReturn = this.betterBets[resultIndex] + ((betShare * this.betterBets[resultIndex]) / this.totalBets[resultIndex]) || 0;
        }

        const totalWinningVotes = this.totalVotes[resultIndex];
        const totalLosingVotes = totalVotesSum - totalWinningVotes;
        const totalVotesShare = totalLosingVotes + arbitrationShare;
        const betterVotesReturn = this.betterVotes[resultIndex] + ((totalVotesShare * this.betterVotes[resultIndex]) / this.totalVotes[resultIndex]) || 0;
        const betterEscrow = this.event.ownerAddress === currentAddress ? this.escrowAmount : 0;
        const totalInvestment = betterBetsSum + betterVotesSum + betterEscrow;
        return {
          address: currentAddress,
          escrow: betterEscrow,
          yourTotalBets: betterBetsSum,
          yourTotalBetsReturn: betterBetsReturn,
          yourTotalBetsReturnRate: (betterBetsReturn / betterBetsSum) * 100,
          yourTotalVotes: betterVotesSum,
          yourTotalVotesReturn: betterVotesReturn,
          yourTotalVotesReturnRate: (betterVotesReturn / betterVotesSum) * 100,
          yourTotalReturn: betterBetsReturn + betterVotesReturn + betterEscrow,
          yourWinningInvestment: this.betterBets[resultIndex] + this.betterVotes[resultIndex],
          yourTotalReturnRate: ((betterBetsReturn + betterVotesReturn + betterEscrow) / totalInvestment) * 100,
        };
      }
      default: {
        throw Error('Invalid event type');
      }
    }
  }

  @computed get remainingConsensusThreshold() {
    const consensusThreshold = parseFloat(this.event.consensusThreshold, 10);
    return toFixed(NP.minus(
      consensusThreshold,
      Number(this.selectedOption.amount)
    ), true);
  }

  constructor(app) {
    this.app = app;
  }

  @action
  reset = () => Object.assign(this, INIT);

  @action
  async init({ txid, url }) {
    this.reset();
    this.txid = txid;
    this.url = url;
    this.setReactions();
    await this.initEvent();
  }

  setReactions = () => {
    // Wallet addresses list changed
    reaction(
      () => toJS(this.app.wallet.addresses),
      async () => {
        if (this.event && this.event.status === WITHDRAWING) {
          await this.queryTotalResultBets();
          await this.calculateWinnings();
          await this.getDidWithdraw();
        }
      }
    );

    // New block
    reaction(
      () => this.app.global.online,
      async () => {
        if (this.app.global.online) await this.initEvent();
      }
    );

    // Tx list, amount, selected option, current wallet address changed
    reaction(
      () => this.transactionHistoryItems
        + this.amount
        + this.selectedOptionIdx
        + this.app.wallet.currentWalletAddress,
      () => this.disableEventActionsIfNecessary(),
      { fireImmediately: true },
    );
    // Update on new block
    reaction(
      () => this.app.global.syncBlockNum,
      async () => {
        if (this.app.history.pendingTransactions.length !== 0 && this.app.ui.location === Routes.EVENT) {
          await this.initEvent();
        }
      },
    );
  }

  @action
  initEvent = async () => {
    if (!this.url) return;
    const roundBetsAddress = this.app.wallet.currentAddress || null;
    const { items } = await events(this.app.graphqlClient, {
      filter: { OR: [{ txid: this.url }, { address: this.url }] },
      includeRoundBets: true,
      roundBetsAddress,
    });
    [this.event] = items;
    if (!this.event) return;
    const { global: { setThemeType } } = this.app;
    setThemeType(eventTypeToAppType(this.event.eventType));
    if (!this.event) return;
    this.address = this.event.address;
    this.escrowAmount = this.event.escrowAmount;

    this.disableEventActionsIfNecessary();
    if (this.isResultSetting) {
      // Set the amount field since we know the amount will be the consensus threshold
      this.amount = this.event.consensusThreshold.toString();
    }

    if (this.isWithdrawing) {
      await this.getDidWithdraw();
      await this.queryPendingWithdraw();
      if (!this.event) return;
      this.selectedOptionIdx = this.event.currentResultIndex;
      await this.queryTotalResultBets();
      await this.calculateWinnings();
      if (!this.event) return;
      if ([
        EVENT_TYPE.AB_EVENT,
        EVENT_TYPE.REWARD_EVENT,
      ].includes(this.event.eventType)) {
        await this.getWithdrawAmounts();
      }
    }
    this.loading = false;
  }

  @action
  getWithdrawAmounts = async () => {
    const address = this.event && this.event.address;
    if (!address || !this.app.wallet.currentAddress) return;
    let getWithdrawAmountsApi;
    switch (this.event.eventType) {
      case EVENT_TYPE.AB_EVENT: {
        getWithdrawAmountsApi = API.GET_WITHDRAW_AMOUNTS;
        break;
      }
      case EVENT_TYPE.REWARD_EVENT: {
        getWithdrawAmountsApi = API.REWARD_GET_WITHDRAW_AMOUNTS;
        break;
      }
      default: {
        throw Error('Invalid event type');
      }
    }
    try {
      const { data } = await axios.get(getWithdrawAmountsApi, {
        params: {
          eventAddress: address,
          address: this.app.wallet.currentAddress,
        },
      });
      const amounts = data.result.map(item => satoshiToDecimal(item));
      [
        this.betterReturn,
        this.voterReturn,
        this.creatorReturn,
      ] = amounts;
    } catch (error) {
      runInAction(() => {
        this.app.globalDialog.setError(
          `${error.message}`,
          getWithdrawAmountsApi,
        );
      });
    }
  }

  @action
  getDidWithdraw = async () => {
    const address = this.event && this.event.address;
    if (!address || !this.app.wallet.currentAddress) return;

    // If we have already called didWithdraw before,
    // only call it again every 5 blocks.
    if (!isUndefined(this.didWithdraw) && this.app.global.syncBlockNum % 5 !== 0) {
      return;
    }

    let didWithdrawApi = API.DID_WITHDRAW;
    switch (this.event.eventType) {
      case EVENT_TYPE.AB_EVENT: {
        didWithdrawApi = API.DID_WITHDRAW;
        break;
      }
      case EVENT_TYPE.MR_EVENT: {
        didWithdrawApi = API.MR_DID_WITHDRAW;
        break;
      }
      case EVENT_TYPE.REWARD_EVENT: {
        didWithdrawApi = API.REWARD_DID_WITHDRAW;
        break;
      }
      default: {
        throw Error('Invalid event type');
      }
    }
    try {
      const { data } = await axios.get(didWithdrawApi, {
        params: {
          eventAddress: address,
          address: this.app.wallet.currentAddress,
        },
      });
      this.didWithdraw = data.result;
    } catch (error) {
      runInAction(() => {
        this.app.globalDialog.setError(
          `${error.message} : ${error.response.data.error}`,
          didWithdrawApi,
        );
      });
    }
  }

  @action
  queryPendingWithdraw = async () => {
    const address = this.event && this.event.address;
    if (!address || !this.app.wallet.currentAddress) return;
    const res = await withdraws(this.app.graphqlClient, {
      filter: { eventAddress: address, txStatus: TransactionStatus.PENDING, winnerAddress: this.app.wallet.currentAddress },
    });
    this.pendingWithdraw = res.items;
  }

  @action
  queryTotalResultBets = async () => {
    const address = this.event && this.event.address;
    if (!address) return;

    const res = await totalResultBets(this.app.graphqlClient, {
      filter: {
        eventAddress: address,
        betterAddress: this.app.wallet.currentAddress,
      },
    });

    this.totalBets = res.totalBets || [];
    this.betterBets = res.betterBets || [];
    this.totalVotes = res.totalVotes || [];
    this.betterVotes = res.betterVotes || [];
    this.totalInvestment = sum(this.betterBets);
  }

  @action
  calculateWinnings = async () => {
    const address = this.event && this.event.address;
    if (!address || !this.app.wallet.currentAddress) return;

    try {
      const res = await eventLeaderboardEntries(this.app.graphqlClient, {
        filter: {
          eventAddress: address,
          userAddress: this.app.wallet.currentAddress,
        },
      });
      if (res.items.length === 0) return;
      this.nbotWinnings = satoshiToDecimal(res.items[0].winnings);
      this.returnRate = ((this.nbotWinnings - this.totalInvestment) / this.totalInvestment) * 100;
      this.profitOrLoss = this.nbotWinnings - this.totalInvestment;
    } catch (error) {
      runInAction(() => {
        this.app.globalDialog.setError(
          `${error.message}`,
          API.CALCULATE_WINNINGS,
        );
      });
    }
  }

  /**
   * These checks represent specific cases where we need to disable the CTA and show a warning message.
   * Blocks the user from doing a tx for this event if any of the cases are hit.
   */
  @action
  disableEventActionsIfNecessary = () => {
    if (!this.event) return;
    const { status, eventType, centralizedOracle, isOpenResultSetting, consensusThreshold } = this.event;
    const { wallet } = this.app;
    const currentWalletNbot = wallet.currentWalletAddress ? wallet.currentWalletAddress.nbot : -1; // when no wallet, still can click on action buttons

    this.buttonDisabled = false;
    this.warningType = '';
    this.eventWarningMessageId = '';
    this.error = INIT.error;

    // Trying to vote over the consensus threshold - currently not way to trigger
    const amountNum = Number(this.amount);
    if (status === ARBITRATION && this.amount && this.selectedOptionIdx >= 0) {
      const maxVote = NP.minus(consensusThreshold, this.selectedOption.amount);
      if (amountNum > maxVote) {
        this.buttonDisabled = true;
        this.error.amount = 'oracle.maxVoteText';
        return;
      }
    }

    // Has not reached betting start time
    if (status === PRE_BETTING) {
      this.buttonDisabled = true;
      this.warningType = EventWarningType.INFO;
      this.eventWarningMessageId = 'oracle.betStartTimeDisabledText';
      return;
    }

    // Has not reached result setting start time
    if (status === PRE_RESULT_SETTING) {
      this.buttonDisabled = true;
      this.warningType = EventWarningType.INFO;
      this.eventWarningMessageId = 'oracle.setStartTimeDisabledText';
      return;
    }

    // User is not the result setter
    if (ORACLE_RESULT_SETTING === status
      && !isOpenResultSetting()
      && wallet.currentAddress
      && centralizedOracle.toLowerCase() !== wallet.currentAddress.toLowerCase()) {
      this.buttonDisabled = true;
      this.warningType = EventWarningType.INFO;
      this.eventWarningMessageId = 'oracle.cOracleDisabledText';
      return;
    }

    // Trying to bet, set result or vote when not enough NBOT
    if (currentWalletNbot >= 0 && (
      (status === BETTING && Number(this.amount) + maxTransactionFee > currentWalletNbot)
      || (status === ARBITRATION && Number(this.amount) + maxTransactionFee > currentWalletNbot)
      || ((status === ORACLE_RESULT_SETTING || status === OPEN_RESULT_SETTING) && currentWalletNbot < consensusThreshold + maxTransactionFee)
    )) {
      this.buttonDisabled = true;
      this.error.amount = 'str.notEnoughNbot';
      return;
    }

    // Trying to bet amount exceeding maxbet allowed
    if (status === BETTING &&
        eventType === EVENT_TYPE.AB_EVENT &&
        Number(this.amount) > this.event.maxBets[this.selectedOptionIdx]) {
      this.buttonDisabled = true;
      this.error.amount = 'str.maxBetText';
      return;
    }

    // Did not select a result
    if (this.selectedOptionIdx === -1) {
      this.buttonDisabled = true;
      this.warningType = EventWarningType.INFO;
      this.eventWarningMessageId = 'oracle.selectResultDisabledText';
      return;
    }

    // Did not enter an amount
    if ([BETTING, ARBITRATION].includes(status) && (!this.amount)) {
      this.buttonDisabled = true;
      this.warningType = EventWarningType.INFO;
      this.eventWarningMessageId = 'oracle.enterAmountDisabledText';
      return;
    }

    // Enter an invalid amount
    if ([BETTING, ARBITRATION].includes(status)
      && (this.amount <= 0 || Number.isNaN(this.amount))) {
      this.buttonDisabled = true;
      this.error.amount = 'str.invalidAmount';
    }
  }

  // Auto-fixes the amount field onBlur if trying to vote over the threshold
  @action
  fixAmount = () => {
    if (this.event.status !== ARBITRATION && this.event.status !== BETTING) return;

    const inputAmount = Number(this.amount);
    const consensusThreshold = Number(this.event.consensusThreshold);
    const maxBet = Number(this.event.maxBets[this.selectedOptionIdx]);
    if (this.event.status === ARBITRATION
      && inputAmount + Number(this.selectedOption.amount) > consensusThreshold) {
      this.amount = String(numbro(NP.minus(
        consensusThreshold,
        Number(this.selectedOption.amount)
      )).format({ mantissa: 8, trimMantissa: true }));
    } else if (this.event.status === BETTING
      && inputAmount > maxBet
      && this.event.eventType === EVENT_TYPE.AB_EVENT) {
      this.amount = String(maxBet);
    }
  }

  @action
  setSelectedOption = (selectedOptionIdx) => {
    if (selectedOptionIdx === this.selectedOptionIdx) {
      this.selectedOptionIdx = INIT.selectedOptionIdx;
    } else {
      this.selectedOptionIdx = selectedOptionIdx;
    }
  }

  bet = async () => {
    if (!this.app.naka.checkLoginAndPopup()) return;

    await this.app.tx.executeBet({
      eventAddr: this.event.address,
      optionIdx: this.selectedOption.idx,
      amount: decimalToSatoshi(this.amount),
      eventRound: this.event.currentRound,
      eventType: this.event.eventType,
    });
    this.setSelectedOption(INIT.selectedOptionIdx);
  }

  deposit = async () => {
    if (!this.app.naka.checkLoginAndPopup()) return;

    await this.app.tx.executeDeposit({
      eventAddr: this.event.address,
      amount: decimalToSatoshi(this.amount),
      eventType: this.event.eventType,
    });
    this.setSelectedOption(INIT.selectedOptionIdx);
  }

  set = async () => {
    if (!this.app.naka.checkLoginAndPopup()) return;

    await this.app.tx.executeSetResult({
      eventAddr: this.event.address,
      optionIdx: this.selectedOption.idx,
      amount: decimalToSatoshi(this.amount),
      eventRound: this.event.currentRound,
      eventType: this.event.eventType,
    });
    this.setSelectedOption(INIT.selectedOptionIdx);
  }

  vote = async () => {
    if (!this.app.naka.checkLoginAndPopup()) return;

    await this.app.tx.executeVote({
      eventAddr: this.event.address,
      optionIdx: this.selectedOption.idx,
      amount: decimalToSatoshi(this.amount),
      eventRound: this.event.currentRound,
      eventType: this.event.eventType,
    });
    this.setSelectedOption(INIT.selectedOptionIdx);
  }

  withdraw = async () => {
    if (!this.app.naka.checkLoginAndPopup()) return;

    let creatorReturnAmount;
    switch (this.event.eventType) {
      case EVENT_TYPE.REWARD_EVENT:
      case EVENT_TYPE.AB_EVENT: {
        creatorReturnAmount = this.creatorReturn;
        break;
      }
      case EVENT_TYPE.MR_EVENT: {
        creatorReturnAmount = this.escrowAmount;
        break;
      }
      default: {
        throw Error('Invalid event type');
      }
    }
    await this.app.tx.executeWithdraw({
      eventAddress: this.event.address,
      winningAmount: decimalToSatoshi(this.nbotWinnings),
      creatorReturnAmount: decimalToSatoshi(creatorReturnAmount),
      version: this.event.version,
      eventType: this.event.eventType,
    });
  }
}

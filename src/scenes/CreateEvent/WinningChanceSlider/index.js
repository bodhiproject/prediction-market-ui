import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withStyles, Slider, Box } from '@material-ui/core';
import { injectIntl, defineMessages, FormattedHTMLMessage } from 'react-intl';
import styles from './styles';
import { Section } from '../components';

const messages = defineMessages({
  winningChance: {
    id: 'create.winningChance',
    defaultMessage: '% Chance of Winning',
  },
});

@withStyles(styles, { withTheme: true })
@injectIntl
@inject('store')
@observer
export default class WinningChanceSlider extends Component {
  onChange = (event, value) => {
    this.props.store.createEvent.setWinningChances(Number(value));
  }

  render() {
    const {
      classes,
      store: { createEvent: { winningChances } },
    } = this.props;
    const marks = [
      {
        value: 1,
        label: '1',
      },
      {
        value: 99,
        label: '99',
      },
    ];
    return (
      <Section column title={messages.winningChance}>
        <Box display="flex" flexDirection="row" justifyContent="space-between">
          <FormattedHTMLMessage
            id="create.winningChanceOutcome"
            defaultMessage={'Outcome {number}: <b>{value}%</b>'}
            values={{ number: 1, value: winningChances[0] }}
          />
          <FormattedHTMLMessage
            id="create.winningChanceOutcome"
            defaultMessage={'Outcome {number}: <b>{value}%</b>'}
            values={{ number: 2, value: winningChances[1] }}
          />
        </Box>
        <Slider
          className={classes.slider}
          min={1}
          max={99}
          value={winningChances[0]}
          step={1}
          onChange={this.onChange}
          marks={marks}
        />
      </Section>
    );
  }
}

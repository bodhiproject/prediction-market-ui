export default (theme) => ({
  slider: {
    flex: 1,
    marginTop: theme.padding.space2X.px,
  },
});

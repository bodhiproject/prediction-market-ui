import React from 'react';
import { observer, inject } from 'mobx-react';
import { injectIntl, defineMessages } from 'react-intl';
import { FormControl, TextField, FormHelperText, withStyles } from '@material-ui/core';
import { Section } from '../components';
import styles from '../styles';

const messages = defineMessages({
  createInitialFundingMsg: {
    id: 'create.initialFunding',
    defaultMessage: 'Initial Funding',
  },
});

const Escrow = ({ classes, store: { createEvent }, intl }) => (
  <Section title={messages.createInitialFundingMsg}>
    <FormControl fullWidth>
      <TextField
        value={createEvent.escrowAmount}
        onChange={e => createEvent.escrowAmount = e.target.value}
        onBlur={createEvent.validateEscrowAmount}
        error={!!createEvent.error.escrow}
        fullWidth
        InputProps={{
          classes: {
            input: classes.textFieldInput,
          },
        }}
      />
      {
        !!createEvent.error.escrow
        && <FormHelperText error>
          {
            intl.formatMessage(
              createEvent.error.escrow,
              { value: createEvent.minEscrowAmount }
            )
          }
        </FormHelperText>
      }
    </FormControl>
  </Section>
);

export default withStyles(styles, { withTheme: true })(injectIntl(inject('store')(observer(Escrow))));

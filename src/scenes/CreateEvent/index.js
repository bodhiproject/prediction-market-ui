import React, { Component, Fragment } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { Button, withStyles, Typography, Tabs, Tab } from '@material-ui/core';
import { Clear, Create } from '@material-ui/icons';
import { FormattedMessage, injectIntl, defineMessages } from 'react-intl';
import { EventWarning as _EventWarning, ImportantNote, Loading, BackButton, PageContainer,
  ContentContainer } from 'components';
import { EventWarningType, APP_TYPE, Routes } from 'constants';
import styles from './styles';
import Escrow from './EscrowField';
import Title from './Title';
import PredictionPeriod from './PredictionPeriod';
import ResultSetPeriod from './ResultSetPeriod';
import Outcomes from './Outcomes';
import ArbitrationRewardSlider from './ArbitrationRewardSlider';
import WinningChanceSlider from './WinningChanceSlider';
import ArbitrationOptionSelector from './ArbitrationOptionSelector';

const messages = defineMessages({
  createEscrowNoteTitleMsg: {
    id: 'create.escrowNoteTitle',
    defaultMessage: '{amount} NBOT Escrow',
  },
  createEscrowNoteDescMsg: {
    id: 'create.escrowNoteDesc',
    defaultMessage: 'You will need to deposit {amount} NBOT in escrow to create an event. You can withdraw it when the event is in the Withdraw stage.',
  },
  pleaseWait: {
    id: 'str.pleasewait',
    defaultMessage: 'Please Wait',
  },
  CLASSIC: {
    id: 'str.classic',
    defaultMessage: 'CLASSIC',
  },
  HOUSE: {
    id: 'str.house',
    defaultMessage: 'HOUSE',
  },
  REWARD: {
    id: 'str.reward',
    defaultMessage: 'REWARD',
  },
});

const TAB_CLASSIC = 0;
const TAB_HOUSE = 1;
const TAB_REWARD = 2;

@injectIntl
@withStyles(styles, { withTheme: true })
@withRouter
@inject('store')
@observer
export default class CreateEvent extends Component {
  componentDidMount() {
    this.props.store.ui.location = Routes.CREATE_EVENT;
    this.props.store.createEvent.close();
    this.props.store.createEvent.init();
  }

  tabIdx = (appType) => (
    {
      [APP_TYPE.CLASSIC]: TAB_CLASSIC,
      [APP_TYPE.HOUSE]: TAB_HOUSE,
      [APP_TYPE.REWARD]: TAB_REWARD,
    }[appType]
  );

  handleTabChange = (event, value) => {
    const { store: { global: { setAppType } } } = this.props;
    switch (value) {
      case TAB_CLASSIC: {
        setAppType(APP_TYPE.CLASSIC);
        break;
      }
      case TAB_HOUSE: {
        setAppType(APP_TYPE.HOUSE);
        break;
      }
      case TAB_REWARD: {
        setAppType(APP_TYPE.REWARD);
        break;
      }
      default: {
        throw new Error(`Invalid tab index: ${value}`);
      }
    }
  }

  render() {
    const {
      store: {
        createEvent: { loaded },
        global: { appType },
      },
      classes,
      intl,
    } = this.props;

    if (!loaded) {
      return <Loading text={messages.pleaseWait} />;
    }

    return (
      <Fragment>
        <BackButton />
        <PageContainer>
          <ContentContainer noSideBar className={classes.createContainer}>
            <Tabs value={this.tabIdx(appType)} onChange={this.handleTabChange} >
              <Tab label={intl.formatMessage(messages[APP_TYPE.CLASSIC])} />
              <Tab label={intl.formatMessage(messages[APP_TYPE.HOUSE])} />
              <Tab label={intl.formatMessage(messages[APP_TYPE.REWARD])} />
            </Tabs>
            <Typography variant="h4" className={classes.title}>
              <FormattedMessage id="str.createEvent" defaultMessage="Create Event" />
            </Typography>
            {
              [APP_TYPE.CLASSIC].includes(appType) &&
                <Fragment>
                  <EscrowAmountNote />
                  <EventWarning />
                </Fragment>
            }
            <Title />
            {
              [APP_TYPE.HOUSE, APP_TYPE.REWARD].includes(appType) &&
                <Fragment>
                  <Escrow />
                  <EventWarning />
                </Fragment>
            }
            <Outcomes />
            {appType === APP_TYPE.HOUSE && <WinningChanceSlider />}
            <PredictionPeriod />
            <ResultSetPeriod />
            {appType !== APP_TYPE.REWARD && <ArbitrationRewardSlider />}
            <ArbitrationOptionSelector />
            <div className={classes.footer}>
              <div className={classes.buttons}>
                <CancelButton />
                <PublishButton />
              </div>
            </div>
          </ContentContainer>
        </PageContainer>
      </Fragment>
    );
  }
}

const EscrowAmountNote = injectIntl(withStyles(styles)(inject('store')(observer(({ classes, intl, store: { createEvent: { escrowAmount } } }) => {
  const heading = intl.formatMessage(messages.createEscrowNoteTitleMsg, { amount: escrowAmount });
  const message = intl.formatMessage(messages.createEscrowNoteDescMsg, { amount: escrowAmount });
  return <ImportantNote className={classes.escrowAmountNote} heading={heading} message={message} />;
}))));

const EventWarning = inject('store')(observer(({ store: { createEvent: { hasEnoughFee, warning } } }) => (
  !hasEnoughFee && <_EventWarning id={warning.id} message={warning.message} type={EventWarningType.ERROR} />
)));

const CancelButton = withStyles(styles)(inject('store')(({ classes, store: { createEvent } }) => (
  <Button className={classes.cancelButton} onClick={createEvent.close} variant="contained" size="small">
    <Clear className={classes.buttonIcon} />
    <FormattedMessage id="str.cancel" defaultMessage="Cancel" />
  </Button>
)));

const PublishButton = withStyles(styles)(withRouter(inject('store')(observer(({ classes, store: { createEvent } }) => (
  <Button
    onClick={() => createEvent.submit()}
    disabled={createEvent.submitting || !createEvent.hasEnoughFee}
    color="primary"
    variant="contained"
    size="small"
  >
    <Create className={classes.buttonIcon} />
    <FormattedMessage id="create.publish" defaultMessage="Publish" />
  </Button>
)))));

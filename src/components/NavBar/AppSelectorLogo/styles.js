export default (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    cursor: 'pointer',
  },
  appTypeContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  expandIcon: {
    width: 30,
    height: 30,
    marginRight: 2,
  },
  type: {
    fontSize: theme.sizes.font.xxxSmall,
    fontWeight: theme.typography.fontWeightBold,
    color: theme.palette.primary.contrastText,
  },
});

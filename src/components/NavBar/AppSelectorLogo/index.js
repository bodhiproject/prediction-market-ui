/* eslint-disable jsx-a11y/media-has-caption */
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withStyles } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import styles from './styles';
import Logo from '../Logo';

let timeout;

@withStyles(styles, { withTheme: true })
@injectIntl
@inject('store')
@observer
export default class AppSelectorLogo extends Component {
  state = {
    clickCount: 0,
  }

  onLogoClick = () => {
    clearTimeout(timeout);
    const { clickCount } = this.state;

    this.setState({ clickCount: clickCount + 1 });
    if (clickCount + 1 === 8) document.getElementsByTagName('audio')[0].play();
    timeout = setTimeout(() => this.setState({ clickCount: 0 }), 500);
  }

  render() {
    const {
      classes,
    } = this.props;

    return (
      <div className={classes.root}>
        <Logo onClick={() => this.onLogoClick()} />
        <audio className={classes.audio} src="/music/bgm.mp3" />
      </div>
    );
  }
}

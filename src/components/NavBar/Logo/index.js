import React from 'react';
import { Link } from 'react-router-dom';
import { Routes } from 'constants';
import ImageLocaleWrapper from '../../ImageLocaleWrapper';

export default ({ onClick }) => (
  <Link to={Routes.PREDICTION}>
    <ImageLocaleWrapper
      appliedLanguages={['zh-Hans-CN']}
      src="/images/bodhi-logo.svg"
      alt="bodhi-logo"
      onClick={onClick}
    />
  </Link>
);

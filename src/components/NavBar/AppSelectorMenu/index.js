import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withStyles, Backdrop } from '@material-ui/core';
import { injectIntl, FormattedMessage } from 'react-intl';
import cx from 'classnames';
import styles from './styles';
import { APP_TYPE } from '../../../constants';

@withStyles(styles, { withTheme: true })
@injectIntl
@inject('store')
@observer
export default class AppSelectorMenu extends Component {
  onClassicClick = () => {
    const {
      store: {
        global: { setAppType },
        ui: { toggleAppSelectorMenu },
      },
    } = this.props;

    setAppType(APP_TYPE.CLASSIC);
    toggleAppSelectorMenu();
  }

  onHouseClick = () => {
    const {
      store: {
        global: { setAppType },
        ui: { toggleAppSelectorMenu },
      },
    } = this.props;

    setAppType(APP_TYPE.HOUSE);
    toggleAppSelectorMenu();
  }

  onRewardClick = () => {
    const {
      store: {
        global: { setAppType },
        ui: { toggleAppSelectorMenu },
      },
    } = this.props;

    setAppType(APP_TYPE.REWARD);
    toggleAppSelectorMenu();
  }

  render() {
    const {
      classes,
      store: {
        ui: { appSelectorMenuOpen, toggleAppSelectorMenu },
      },
    } = this.props;

    return (
      <div className={cx(classes.root, appSelectorMenuOpen ? '' : 'hide')}>
        <div
          className={classes.item}
          onClick={this.onClassicClick}
        >
          <FormattedMessage id="navbar.classic" defaultMessage="Classic" />
        </div>
        <div
          className={classes.item}
          onClick={this.onHouseClick}
        >
          <FormattedMessage id="navbar.house" defaultMessage="House" />
        </div>
        <div
          className={classes.item}
          onClick={this.onRewardClick}
        >
          <FormattedMessage id="navbar.reward" defaultMessage="Reward" />
        </div>
        <Backdrop
          invisible
          open={appSelectorMenuOpen}
          onClick={toggleAppSelectorMenu}
        />
      </div>
    );
  }
}

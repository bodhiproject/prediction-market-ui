export default (theme) => ({
  root: {
    minWidth: 135,
    background: theme.palette.background.paper,
    boxShadow: '0px -2px 20px -2px rgba(0,0,0,0.2), 0px -2px 5px rgba(0,0,0,0.1)',
    fontSize: theme.sizes.font.xSmall,
    position: 'absolute',
    left: 0,
    top: theme.sizes.navHeight.px,
    transition: '0.3s all ease-in-out',
    '&.hide': {
      display: 'none',
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: theme.sizes.font.xxSmall,
    },
  },
  item: {
    color: theme.palette.text.primary,
    background: theme.palette.background.paper,
    display: 'flex',
    textAlign: 'center',
    padding: theme.padding.space3X.px,
    cursor: 'pointer',
    borderBottom: theme.border,
    justifyContent: 'space-between',
    '&:hover': {
      background: theme.palette.background.grey,
    },
    [theme.breakpoints.down('xs')]: {
      padding: theme.padding.space2X.px,
    },
  },
});

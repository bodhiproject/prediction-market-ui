import { Token, EVENT_STATUS } from 'constants';

const { NBOT } = Token;

// similar to Option model, intended for showing as an option in event page
export default class HouseFunding {
  name
  value
  userValue
  amount
  phase
  token
  idx
  isHouseFunding

  constructor(i, event) {
    this.idx = i;
    this.amount = event.escrowAmount || 0;
    this.isLast = event.status === EVENT_STATUS.BETTING && i === event.results.length - 1;
    this.isFirst = i === 0;
    this.name = 'House Funding';
    this.token = NBOT;
    this.phase = event.phase;
    this.value = this.amount;
    this.userValue = event.escrowAmount || 0;
    this.userPercent = 100;
    this.isHouseFunding = true;
  }

  isExpanded = (selectedOptionIdx) => selectedOptionIdx === this.idx
}

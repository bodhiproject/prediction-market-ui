import { satoshiToDecimal } from '../helpers/utility';

export default class Deposit {
  txid // Transaction ID returned when confirmed
  txStatus // One of: [PENDING, SUCCESS, FAIL]
  txReceipt // Transaction receipt returned when confirmed
  blockNum // Block number when executed
  block // Block info returned when confirmed
  eventAddress // Event contract address
  depositorAddress // Depositor's address
  depositorName // Depositor's name
  txSender // Transaction sender address, same as above
  eventType // Type of this deposit event

  amount // Bet amount in decimals
  amountSatoshi // Bet amount in satoshi

  constructor(deposit) {
    Object.assign(this, deposit);
    this.amount = satoshiToDecimal(deposit.amount);
    this.amountSatoshi = deposit.amount;
    this.txSender = deposit.depositorAddress;
  }
}

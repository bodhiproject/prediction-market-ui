import _ from 'lodash';
import { Token, EVENT_STATUS, EVENT_TYPE } from 'constants';
import { decimalToSatoshi } from '../helpers/utility';

const { NBOT } = Token;

export default class Option {
  name
  value
  userValue
  percent
  isPrevResult
  maxAmount
  amount
  phase
  token
  idx
  isBetting
  odds

  constructor(optionName, i, event) {
    this.idx = i;
    this.amount = event.roundBets[i] || 0;
    this.isLast = i === event.results.length - 1;
    this.isFirst = i === 0;
    this.name = optionName;
    this.token = NBOT;
    this.phase = event.phase;
    this.value = this.amount;
    this.isHiding = false;
    if (event.currentRound === 0) {
      const totalBalance = _.sum(event.roundBets);
      this.percent = totalBalance === 0 ? totalBalance : _.round((this.amount / totalBalance) * 100, 2);
      this.userPercent = this.amount === 0 ? this.amount : _.round((event.userRoundBets[i] / this.amount) * this.percent, 2);
      this.userValue = event.userRoundBets[i];
      switch (event.eventType) {
        case EVENT_TYPE.AB_EVENT: {
          this.odds = event.odds[i];
          this.maxBet = event.maxBets[i];
          this.isHiding = [EVENT_STATUS.PRE_BETTING, EVENT_STATUS.BETTING].includes(event.status) && this.odds <= 1;
          break;
        }
        case EVENT_TYPE.MR_EVENT: {
          if (this.value === 0) this.odds = undefined;
          else {
            const betRoundSum = _.sum(event.betRoundBets);
            this.odds = 1 + (((betRoundSum - event.betRoundBets[i]) * (1 - (event.arbitrationRewardPercentage / 100))) / this.value);
          }
          break;
        }
        case EVENT_TYPE.REWARD_EVENT: {
          if (this.value === 0) this.odds = undefined;
          else {
            this.odds = 1 + ((event.escrowAmount * (1 - (event.arbitrationRewardPercentage / 100))) / event.betRoundBets[i]);
          }
          break;
        }
        default: {
          throw Error('Invalid event type');
        }
      }
    } else {
      this.isPrevResult = event.currentResultIndex === i;
      if (this.isPrevResult) {
        this.percent = 100;
        if (event.currentRound === 1) {
          this.value = this.amount;
          this.userPercent = _.round((event.userRoundBets[i] / event.roundBets[i]) * this.percent, 2);
          this.userValue = event.userRoundBets[i];
        } else {
          this.value = event.previousRoundBets[i];
          this.userPercent = _.round((event.previousRoundUserBets[i] / event.previousRoundBets[i]) * this.percent, 2);
          this.userValue = event.previousRoundUserBets[i];
        }
      } else {
        const threshold = event.consensusThreshold;
        this.value = this.amount;
        this.percent = _.round((this.amount / threshold) * 100, 2);
        this.userPercent = this.amount === 0 ? this.amount : _.round((event.userRoundBets[i] / this.amount) * this.percent, 2);
        this.userValue = event.userRoundBets[i];
      }
      this.maxAmount = event.status === EVENT_STATUS.ARBITRATION
        ? event.consensusThreshold - decimalToSatoshi(this.amount) : undefined;
    }
    this.disabled = this.isPrevResult;
    this.isBetting = event.currentRound === 0;
  }

  isExpanded = (selectedOptionIdx) => selectedOptionIdx === this.idx
}

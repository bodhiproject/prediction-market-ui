import { action, toJS } from 'mobx';
import promisify from 'js-promisify';
import Web3 from 'web3';
import { utf8ToHex, padRight } from 'web3-utils';
import { cloneDeep, map, assign } from 'lodash';
import { EVENT_TYPE, EVENT_TX_TYPE } from 'constants';
import logger from 'loglevel';
import {
  addPendingEvent,
  addPendingBet,
  addPendingDeposit,
  addPendingResultSet,
  addPendingWithdraw,
} from '../network/graphql/mutations';
import {
  NakaBodhiToken,
  EventFactory,
  getEventMeta,
} from '../config/contracts';
import Tracking from '../helpers/mixpanelUtil';

const CREATE_MR_EVENT_FUNC_SIG = '67d0df4c';
const CREATE_AB_EVENT_FUNC_SIG = '5280f033';
const CREATE_REWARD_EVENT_FUNC_SIG = '5280f033';
const DEPOSIT_FUNC_SIG = '47e7ef24';
const BET_FUNC_SIG = '885ab66d';
const SET_RESULT_FUNC_SIG = 'a6b4218b';
const VOTE_FUNC_SIG = '1e00eb7f';

const CREATE_MR_EVENT_FUNC_TYPES = [
  'string',
  'bytes32[3]',
  'uint256',
  'uint256',
  'address',
  'uint8',
  'uint256',
];
const CREATE_AB_EVENT_FUNC_TYPES = [
  'string',
  'bytes32[2]',
  'uint256',
  'uint256',
  'address',
  'uint8',
  'uint256',
  'uint8[2]',
];
const CREATE_REWARD_EVENT_FUNC_TYPES = [
  'string',
  'bytes32[]',
  'uint256',
  'uint256',
  'address',
  'uint8',
  'uint256',
];
const EVENT_ACTION_FUNC_TYPES = [
  'uint8',
];

const web3 = new Web3();

export default class TransactionStore {
  app = undefined;

  constructor(app) {
    this.app = app;
  }

  /**
   * For mobile flow. Executes post-tx logic since the logic stops after going to
   * the confirm tx mobile screen.
   * @param {string} txid Transaction ID
   * @param {object} tx Event tx object
   * @param {Error} err Error object
   */
  onTxExecuted = async (txid, tx, err) => {
    if (err) {
      console.error('onTxExecuted error:', err);
      return;
    }

    console.log('onTxExecuted running');
    const { txType } = tx;
    delete tx.txType; // Mutations don't need this field
    tx.txid = txid; // Set txid in tx

    const { graphqlClient, createEvent } = this.app;
    let res;
    switch (txType) {
      case EVENT_TX_TYPE.CREATE_EVENT: {
        res = await addPendingEvent(graphqlClient, tx);
        break;
      }
      case EVENT_TX_TYPE.BET:
      case EVENT_TX_TYPE.VOTE: {
        res = await addPendingBet(graphqlClient, tx);
        break;
      }
      case EVENT_TX_TYPE.DEPOSIT: {
        res = await addPendingDeposit(graphqlClient, tx);
        break;
      }
      case EVENT_TX_TYPE.SET_RESULT: {
        res = await addPendingResultSet(graphqlClient, tx);
        break;
      }
      case EVENT_TX_TYPE.WITHDRAW: {
        res = await addPendingWithdraw(graphqlClient, tx);
        break;
      }
      default: {
        throw Error(`Invalid txType: ${tx.txType}`);
      }
    }
    if (res) await this.afterTxExecuted(res);
    if (txType === EVENT_TX_TYPE.CREATE_EVENT) createEvent.onEventCreated();
  }

  handleReqError = (err, reqName) => {
    const { globalDialog } = this.app;
    if (err.networkError
      && err.networkError.result.errors
      && err.networkError.result.errors.length > 0) {
      // Handles GraphQL error
      globalDialog.setError(
        `${err.message} : ${err.networkError.result.errors[0].message}`,
        `GraphQL error: ${reqName}`,
      );
    } else {
      // Handle other error
      globalDialog.setError(err.message, `Other error: ${reqName}`);
    }
  }

  getPayByTokenParams = () => {
    const { nbotOwner, exchangeRate } = this.app.wallet;
    if (!nbotOwner) throw Error('exchanger not defined');
    if (!exchangeRate) throw Error('exchangeRate not defined');

    return {
      token: NakaBodhiToken()[this.app.naka.network.toLowerCase()],
      exchanger: nbotOwner,
      exchangeRate,
    };
  }

  sendCreateEventTx = async ({
    nbotMethods,
    eventFactoryAddr,
    eventType,
    eventParams,
    escrowAmt,
    gas,
    txObj,
  }) => {
    try {
      // Validate EventFactory address
      if (!eventFactoryAddr) throw Error('Missing eventFactoryAddr');

      // Set function types and signature based on event type
      let createEventFuncTypes;
      let createEventFuncSig;
      switch (eventType) {
        case EVENT_TYPE.AB_EVENT: {
          createEventFuncTypes = CREATE_AB_EVENT_FUNC_TYPES;
          createEventFuncSig = CREATE_AB_EVENT_FUNC_SIG;
          break;
        }
        case EVENT_TYPE.MR_EVENT: {
          createEventFuncTypes = CREATE_MR_EVENT_FUNC_TYPES;
          createEventFuncSig = CREATE_MR_EVENT_FUNC_SIG;
          break;
        }
        case EVENT_TYPE.REWARD_EVENT: {
          createEventFuncTypes = CREATE_REWARD_EVENT_FUNC_TYPES;
          createEventFuncSig = CREATE_REWARD_EVENT_FUNC_SIG;
          break;
        }
        default: {
          throw Error('Invalid event type');
        }
      }

      // Construct params
      const paramsHex = web3.eth.abi.encodeParameters(
        createEventFuncTypes,
        eventParams,
      ).substr(2);
      const data = `0x${createEventFuncSig}${paramsHex}`;

      // Send tx
      const pbtParams = this.getPayByTokenParams();
      const txid = await promisify(
        nbotMethods.transfer['address,uint256,bytes'].sendTransaction,
        [eventFactoryAddr, escrowAmt, data, {
          gas,
          ...pbtParams,
          pmTx: JSON.stringify(txObj),
        }]
      );
      return txid;
    } catch (err) {
      logger.error('TransactionStore.sendCreateEventTx', err);
      return undefined;
    }
  };

  sendTransfer223Tx = async ({
    nbotMethods,
    eventAddr,
    funcSig,
    params,
    amount,
    gas,
    txObj,
  }) => {
    try {
      // Construct params
      let data;
      if (params) {
        const paramsHex = web3.eth.abi.encodeParameters(
          EVENT_ACTION_FUNC_TYPES,
          params,
        ).substr(2);
        data = `0x${funcSig}${paramsHex}`;
      } else {
        data = `0x${funcSig}`;
      }

      // Send tx
      const pbtParams = this.getPayByTokenParams();
      const txid = await promisify(
        nbotMethods.transfer['address,uint256,bytes'].sendTransaction,
        [eventAddr, amount, data, {
          gas,
          ...pbtParams,
          pmTx: JSON.stringify(txObj),
        }]
      );
      return txid;
    } catch (err) {
      logger.error('TransactionStore.sendTransfer223Tx', err);
    }
  };

  /**
   * Logic to execute after a tx has been executed.
   * @param {object} pendingTx Pending tx after mutation
   */
  @action
  afterTxExecuted = async (pendingTx) => {
    const {
      history: { toggleHistoryNeedUpdate },
      global: { toggleBalanceNeedUpdate },
    } = this.app;
    // Refresh detail page if one the same page
    if (pendingTx.eventAddress
      && pendingTx.eventAddress === this.app.eventPage.address) {
      this.app.history.addPendingTx(pendingTx);
    }
    toggleHistoryNeedUpdate();

    // Trigger balance fetch on next block
    toggleBalanceNeedUpdate();
  }

  /**
   * Executes a create event.
   * @param {object} params Create event params.
   */
  @action
  executeCreateEvent = async (params) => {
    try {
      // Analytics tracking
      Tracking.track('event-createEvent');

      const {
        senderAddress,
        centralizedOracle,
        name,
        betEndTime,
        resultSetStartTime,
        amountSatoshi,
        arbitrationOptionIndex,
        arbitrationRewardPercentage,
        winningChances,
        language,
        eventType,
      } = params;

      // Adjust results array based on event type
      let results = cloneDeep(params.results);
      if (eventType === EVENT_TYPE.MR_EVENT) {
        results = assign(new Array(3).fill(''), results);
      } else if (eventType === EVENT_TYPE.AB_EVENT) {
        results = assign(new Array(2).fill(''), results);
      }
      // Convert results to padded hex
      results = map(results, res => padRight(utf8ToHex(res), 64));

      // Construct obj
      const eventObj = {
        ownerAddress: senderAddress,
        name,
        results,
        numOfResults: results.length,
        centralizedOracle,
        betEndTime,
        resultSetStartTime,
        language,
        eventType,
      };

      // Execute tx
      const eventParams = [
        name,
        results,
        betEndTime,
        resultSetStartTime,
        centralizedOracle,
        arbitrationOptionIndex,
        arbitrationRewardPercentage,
      ];
      if (eventType === EVENT_TYPE.AB_EVENT) eventParams.push(toJS(winningChances));
      const { network } = this.app.naka;
      const nbotMethods = window.naka.eth.contract(NakaBodhiToken().abi)
        .at(NakaBodhiToken()[network.toLowerCase()]);
      const txid = await this.sendCreateEventTx({
        nbotMethods,
        eventFactoryAddr: EventFactory(eventType)[network.toLowerCase()],
        eventType,
        eventParams,
        escrowAmt: amountSatoshi,
        gas: 4700000,
        txObj: {
          txType: EVENT_TX_TYPE.CREATE_EVENT,
          ...eventObj,
        },
      });
      if (txid) {
        // Create pending tx on server
        eventObj.txid = txid;
        const res = await addPendingEvent(this.app.graphqlClient, eventObj);
        await this.afterTxExecuted(res);
        return txid;
      }
      return undefined;
    } catch (err) {
      logger.error('TransactionStore.executeCreateEvent', err);
      this.handleReqError(err, 'addPendingEvent');
    }
  }

  /**
   * Executes a deposit.
   * @param {object} params Deposit params.
   */
  @action
  executeDeposit = async (params) => {
    try {
      // Analytics tracking
      Tracking.track('event-deposit');

      // Construct obj
      const { eventAddr, amount, eventType } = params;
      const depositObj = {
        eventAddress: eventAddr,
        depositorAddress: this.app.wallet.currentWalletAddress.address,
        amount,
        eventType,
      };

      // Execute tx
      const nbotMethods = window.naka.eth.contract(NakaBodhiToken().abi)
        .at(NakaBodhiToken()[this.app.naka.network.toLowerCase()]);
      const txid = await this.sendTransfer223Tx({
        nbotMethods,
        eventAddr,
        funcSig: DEPOSIT_FUNC_SIG,
        amount,
        gas: 300000,
        txObj: {
          txType: EVENT_TX_TYPE.DEPOSIT,
          ...depositObj,
        },
      });
      if (txid) {
        // Create pending tx on server
        depositObj.txid = txid;
        const res = await addPendingDeposit(this.app.graphqlClient, depositObj);
        await this.afterTxExecuted(res);
      }
    } catch (err) {
      logger.error('TransactionStore.executeDeposit', err);
      this.handleReqError(err, 'addPendingDeposit');
    }
  }

  /**
   * Executes a bet.
   * @param {object} params Bet params.
   */
  @action
  executeBet = async (params) => {
    try {
      // Analytics tracking
      Tracking.track('event-bet');

      // Construct obj
      const { eventAddr, optionIdx, amount, eventRound, eventType } = params;
      const betObj = {
        eventAddress: eventAddr,
        betterAddress: this.app.wallet.currentWalletAddress.address,
        resultIndex: optionIdx,
        amount,
        eventRound,
        eventType,
      };

      // Execute tx
      const nbotMethods = window.naka.eth.contract(NakaBodhiToken().abi)
        .at(NakaBodhiToken()[this.app.naka.network.toLowerCase()]);
      const txid = await this.sendTransfer223Tx({
        nbotMethods,
        eventAddr,
        funcSig: BET_FUNC_SIG,
        params: [optionIdx],
        amount,
        gas: 300000,
        txObj: {
          txType: EVENT_TX_TYPE.BET,
          ...betObj,
        },
      });
      if (txid) {
        // Create pending tx on server
        betObj.txid = txid;
        const res = await addPendingBet(this.app.graphqlClient, betObj);
        await this.afterTxExecuted(res);
      }
    } catch (err) {
      logger.error('TransactionStore.executeBet', err);
      this.handleReqError(err, 'addPendingBet');
    }
  }

  /**
   * Executes a set result.
   * @param {object} params Set Result params.
   */
  @action
  executeSetResult = async (params) => {
    try {
      // Analytics tracking
      Tracking.track('event-setResult');

      // Construct obj
      const { eventAddr, optionIdx, amount, eventRound, eventType } = params;
      const setObj = {
        eventAddress: eventAddr,
        centralizedOracleAddress: this.app.wallet.currentWalletAddress.address,
        resultIndex: optionIdx,
        amount,
        eventRound,
        eventType,
      };

      // Execute tx
      const nbotMethods = window.naka.eth.contract(NakaBodhiToken().abi)
        .at(NakaBodhiToken()[this.app.naka.network.toLowerCase()]);
      const txid = await this.sendTransfer223Tx({
        nbotMethods,
        eventAddr,
        funcSig: SET_RESULT_FUNC_SIG,
        params: [optionIdx],
        amount,
        gas: 500000,
        txObj: {
          txType: EVENT_TX_TYPE.SET_RESULT,
          ...setObj,
        },
      });
      if (txid) {
        // Create pending tx on server
        setObj.txid = txid;
        const res = await addPendingResultSet(this.app.graphqlClient, setObj);
        await this.afterTxExecuted(res);
      }
    } catch (err) {
      logger.error('TransactionStore.executeSetResult', err);
      this.handleReqError(err, 'addPendingResultSet');
    }
  }

  /**
   * Executes a vote.
   * @param {object} params Vote params.
   */
  @action
  executeVote = async (params) => {
    try {
      // Analytics tracking
      Tracking.track('event-vote');

      // Construct obj
      const { eventAddr, optionIdx, amount, eventRound, eventType } = params;
      const voteObj = {
        eventAddress: eventAddr,
        betterAddress: this.app.wallet.currentWalletAddress.address,
        resultIndex: optionIdx,
        amount,
        eventRound,
        eventType,
      };

      // Execute tx
      const nbotMethods = window.naka.eth.contract(NakaBodhiToken().abi)
        .at(NakaBodhiToken()[this.app.naka.network.toLowerCase()]);
      const txid = await this.sendTransfer223Tx({
        nbotMethods,
        eventAddr,
        funcSig: VOTE_FUNC_SIG,
        params: [optionIdx],
        amount,
        gas: 300000,
        txObj: {
          txType: EVENT_TX_TYPE.VOTE,
          ...voteObj,
        },
      });
      if (txid) {
        // Create pending tx on server
        voteObj.txid = txid;
        const res = await addPendingBet(this.app.graphqlClient, voteObj);
        await this.afterTxExecuted(res);
      }
    } catch (err) {
      logger.error('TransactionStore.executeVote', err);
      this.handleReqError(err, 'addPendingBet');
    }
  }

  /**
   * Executes a withdraw.
   * @param {object} params Withdraw params.
   */
  @action
  executeWithdraw = async (params) => {
    try {
      // Analytics tracking
      Tracking.track('event-withdraw');

      // Construct obj
      const { eventAddress, winningAmount, creatorReturnAmount, version, eventType } = params;
      const withdrawObj = {
        eventAddress,
        winnerAddress: this.app.wallet.currentWalletAddress.address,
        winningAmount,
        creatorReturnAmount,
        eventType,
      };

      // Execute tx
      const nbotMethods = window.naka.eth.contract(getEventMeta(eventType, version).abi)
        .at(eventAddress);
      const pbtParams = this.getPayByTokenParams();
      const txid = await promisify(nbotMethods.withdraw, [{
        gas: 200000,
        ...pbtParams,
        pmTx: JSON.stringify({
          txType: EVENT_TX_TYPE.WITHDRAW,
          ...withdrawObj,
        }),
      }]);
      if (txid) {
        // Create pending tx on server
        withdrawObj.txid = txid;
        const res = await addPendingWithdraw(this.app.graphqlClient, withdrawObj);
        await this.afterTxExecuted(res);
      }
    } catch (err) {
      logger.error('TransactionStore.executeWithdraw', err);
      this.handleReqError(err, 'addPendingWithdraw');
    }
  }
}

const SSL = process.env.SSL === 'true';
const HOSTNAME = process.env.API_HOSTNAME;
const HTTP_ROUTE = `${SSL ? 'https' : 'http'}://${HOSTNAME}`;
const WS_ROUTE = `${SSL ? 'wss' : 'ws'}://${HOSTNAME}/graphql`;
const EXPLORER_URL = process.env.NETWORK === 'mainnet'
  ? 'https://explorer.nakachain.org'
  : 'https://testnet.explorer.nakachain.org';

export const GRAPHQL = {
  HTTP: `${HTTP_ROUTE}/graphql`,
  SUBS: WS_ROUTE,
};

export const API = {
  // log
  LOG_CLIENT_ERROR: `${HTTP_ROUTE}/log/client-error`,

  // config-manager
  EVENT_FACTORY_ADDRESS: `${HTTP_ROUTE}/config-manager/event-factory-address`,
  EVENT_ESCROW_AMOUNT: `${HTTP_ROUTE}/config-manager/event-escrow-amount`,
  ARBITRATION_LENGTH: `${HTTP_ROUTE}/config-manager/arbitration-length`,
  STARTING_CONSENSUS_THRESHOLD: `${HTTP_ROUTE}/config-manager/starting-consensus-threshold`,
  THRESHOLD_PERCENT_INCREASE: `${HTTP_ROUTE}/config-manager/threshold-percent-increase`,

  // ab-event
  CALCULATE_WINNINGS: `${HTTP_ROUTE}/ab-event/calculate-winnings`,
  GET_WITHDRAW_AMOUNTS: `${HTTP_ROUTE}/ab-event/get-withdraw-amounts`,
  VERSION: `${HTTP_ROUTE}/ab-event/version`,
  ROUND: `${HTTP_ROUTE}/ab-event/round`,
  RESULT_INDEX: `${HTTP_ROUTE}/ab-event/result-index`,
  CONSENSUS_THRESHOLD: `${HTTP_ROUTE}/ab-event/consensus-threshold`,
  ARBITRATION_END_TIME: `${HTTP_ROUTE}/ab-event/arbitration-end-time`,
  EVENT_METADATA: `${HTTP_ROUTE}/ab-event/event-metadata`,
  CENTRALIZED_METADATA: `${HTTP_ROUTE}/ab-event/centralized-metadata`,
  CONFIG_METADATA: `${HTTP_ROUTE}/ab-event/config-metadata`,
  TOTAL_BETS: `${HTTP_ROUTE}/ab-event/total-bets`,
  DID_WITHDRAW: `${HTTP_ROUTE}/ab-event/did-withdraw`,

  // mr-event
  MR_CALCULATE_WINNINGS: `${HTTP_ROUTE}/mr-event/calculate-winnings`,
  MR_VERSION: `${HTTP_ROUTE}/mr-event/version`,
  MR_ROUND: `${HTTP_ROUTE}/mr-event/round`,
  MR_RESULT_INDEX: `${HTTP_ROUTE}/mr-event/result-index`,
  MR_CONSENSUS_THRESHOLD: `${HTTP_ROUTE}/mr-event/consensus-threshold`,
  MR_ARBITRATION_END_TIME: `${HTTP_ROUTE}/mr-event/arbitration-end-time`,
  MR_EVENT_METADATA: `${HTTP_ROUTE}/mr-event/event-metadata`,
  MR_CENTRALIZED_METADATA: `${HTTP_ROUTE}/mr-event/centralized-metadata`,
  MR_CONFIG_METADATA: `${HTTP_ROUTE}/mr-event/config-metadata`,
  MR_TOTAL_BETS: `${HTTP_ROUTE}/mr-event/total-bets`,
  MR_DID_WITHDRAW: `${HTTP_ROUTE}/mr-event/did-withdraw`,
  MR_DID_WITHDRAW_ESCROW: `${HTTP_ROUTE}/mr-event/did-withdraw-escrow`,

  // reward-event
  REWARD_CALCULATE_WINNINGS: `${HTTP_ROUTE}/reward-event/calculate-winnings`,
  REWARD_GET_WITHDRAW_AMOUNTS: `${HTTP_ROUTE}/reward-event/get-withdraw-amounts`,
  REWARD_VERSION: `${HTTP_ROUTE}/reward-event/version`,
  REWARD_ROUND: `${HTTP_ROUTE}/reward-event/round`,
  REWARD_RESULT_INDEX: `${HTTP_ROUTE}/reward-event/result-index`,
  REWARD_CONSENSUS_THRESHOLD: `${HTTP_ROUTE}/reward-event/consensus-threshold`,
  REWARD_ARBITRATION_END_TIME: `${HTTP_ROUTE}/reward-event/arbitration-end-time`,
  REWARD_EVENT_METADATA: `${HTTP_ROUTE}/reward-event/event-metadata`,
  REWARD_CENTRALIZED_METADATA: `${HTTP_ROUTE}/reward-event/centralized-metadata`,
  REWARD_CONFIG_METADATA: `${HTTP_ROUTE}/reward-event/config-metadata`,
  REWARD_TOTAL_BETS: `${HTTP_ROUTE}/reward-event/total-bets`,
  REWARD_DID_WITHDRAW: `${HTTP_ROUTE}/reward-event/did-withdraw`,
};

export const EXPLORER = {
  TX: `${EXPLORER_URL}/tx`,
};

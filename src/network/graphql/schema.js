export const NEXT_TRANSACTION_SKIPS = `
  nextEventSkip
  nextDepositSkip
  nextBetSkip
  nextResultSetSkip
  nextWithdrawSkip
`;

export const PAGE_INFO = `
  hasNextPage
  pageNumber
  count
  nextTransactionSkips { ${NEXT_TRANSACTION_SKIPS} }
`;

export const BLOCK = `
  blockNum
  blockTime
`;

export const TRANSACTION_RECEIPT = `
  status
  blockHash
  blockNum
  transactionHash
  from
  to
  contractAddress
  cumulativeGasUsed
  gasUsed
  gasPrice
`;

export const PENDING_TRANSACTIONS = `
  bet
  resultSet
  withdraw
  total
`;

export const ROUND_BETS = `
  singleUserRoundBets
  singleTotalRoundBets
`;

// Transaction interface
export const ITRANSACTION = `
  txType
  txid
  txStatus
  txReceipt { ${TRANSACTION_RECEIPT} }
  blockNum
  block { ${BLOCK} }
`;

// Event extends Transaction
export const EVENT = `
  ${ITRANSACTION}
  address
  ownerAddress
  version
  name
  results
  numOfResults
  centralizedOracle
  betStartTime
  betEndTime
  resultSetStartTime
  resultSetEndTime
  escrowAmount
  arbitrationLength
  thresholdPercentIncrease
  arbitrationRewardPercentage
  currentRound
  currentResultIndex
  consensusThreshold
  previousConsensusThreshold
  arbitrationEndTime
  status
  language
  eventType
  pendingTxs { ${PENDING_TRANSACTIONS} }
  roundBets { ${ROUND_BETS} }
  totalBets
  withdrawnList
  maxBets
  odds
`;

export const PAGINATED_EVENTS = `
  totalCount
  pageInfo { ${PAGE_INFO} }
  items { ${EVENT} }
`;

// Bet extends Transaction
export const BET = `
  ${ITRANSACTION}
  eventAddress
  betterAddress
  resultIndex
  amount
  eventRound
  resultName
  eventName
  eventType
`;

export const PAGINATED_BETS = `
  totalCount
  pageInfo { ${PAGE_INFO} }
  items { ${BET} }
`;

// Deposit extends Transaction
export const DEPOSIT = `
  ${ITRANSACTION}
  eventAddress
  depositorAddress
  amount
  eventName
  eventType
`;

export const PAGINATED_DEPOSITS = `
  totalCount
  pageInfo { ${PAGE_INFO} }
  items { ${DEPOSIT} }
`;

// ResultSet extends Transaction
export const RESULT_SET = `
  ${ITRANSACTION}
  eventAddress
  centralizedOracleAddress
  resultIndex
  amount
  eventRound
  resultName
  eventName
  eventType
`;

export const PAGINATED_RESULT_SETS = `
  totalCount
  pageInfo { ${PAGE_INFO} }
  items { ${RESULT_SET} }
`;

// Withdraw extends Transaction
export const WITHDRAW = `
  ${ITRANSACTION}
  eventAddress
  winnerAddress
  winningAmount
  creatorReturnAmount
  eventName
  eventType
`;

export const PAGINATED_WITHDRAWS = `
  totalCount
  pageInfo { ${PAGE_INFO} }
  items { ${WITHDRAW} }
`;

export const PAGINATED_TRANSACTIONS = `
  totalCount
  pageInfo { ${PAGE_INFO} }
  items {
    ${ITRANSACTION}
    ... on Event {
      address
      ownerAddress
      ownerName
      name
      escrowAmount
      eventType
    }
    ... on Deposit {
      eventAddress
      depositorAddress
      depositorName
      amount
      eventName
      eventType
    }
    ... on Bet {
      eventAddress
      betterAddress
      betterName
      resultIndex
      amount
      eventRound
      resultName
      eventName
      eventType
    }
    ... on ResultSet {
      eventAddress
      centralizedOracleAddress
      centralizedOracleName
      resultIndex
      amount
      eventRound
      resultName
      eventName
      eventType
    }
    ... on Withdraw {
      eventAddress
      winnerAddress
      winnerName
      winningAmount
      creatorReturnAmount
      eventName
      eventType
    }
  }
`;

export const SYNC_INFO = `
  syncBlockNum
  syncBlockTime
  syncPercent
`;

export const TOTAL_RESULT_BETS = `
  totalBets
  betterBets
  totalVotes
  betterVotes
`;

export const ALL_STATS = `
  eventCount
  participantCount
  totalBets
`;

export const LEADERBOARD_ENTRY = `
  eventAddress
  userAddress
  userName
  investments
  winnings
  returnRatio
`;

export const PAGINATED_LEADERBOARD_ENTRY = `
  totalCount
  pageInfo { ${PAGE_INFO} }
  items { ${LEADERBOARD_ENTRY} }
`;

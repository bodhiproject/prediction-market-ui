import { gql } from 'apollo-boost';
import { Event, Bet, ResultSet, Withdraw, Deposit } from 'models';
import { EVENT, BET, RESULT_SET, WITHDRAW, DEPOSIT } from './schema';

const MUTA_ADD_PENDING_EVENT = 'addPendingEvent';
const MUTA_ADD_PENDING_BET = 'addPendingBet';
const MUTA_ADD_PENDING_DEPOSIT = 'addPendingDeposit';
const MUTA_ADD_PENDING_RESULT_SET = 'addPendingResultSet';
const MUTA_ADD_PENDING_WITHDRAW = 'addPendingWithdraw';

const MUTATIONS = {
  addPendingEvent: gql`
    mutation(
      $txid: String!
      $ownerAddress: String!
      $name: String!
      $results: [String!]!
      $numOfResults: Int!
      $centralizedOracle: String!
      $betEndTime: Int!
      $resultSetStartTime: Int!
      $language: String!
      $eventType: EventType!
    ) {
      addPendingEvent(
        txid: $txid
        ownerAddress: $ownerAddress
        name: $name
        results: $results
        numOfResults: $numOfResults
        centralizedOracle: $centralizedOracle
        betEndTime: $betEndTime
        resultSetStartTime: $resultSetStartTime
        language: $language
        eventType: $eventType
      ) {
        ${EVENT}
      }
    }
  `,

  addPendingBet: gql`
    mutation(
      $txid: String!
      $eventAddress: String!
      $betterAddress: String!
      $resultIndex: Int!
      $amount: String!
      $eventRound: Int!
      $eventType: EventType!
    ) {
      addPendingBet(
        txid: $txid
        eventAddress: $eventAddress
        betterAddress: $betterAddress
        resultIndex: $resultIndex
        amount: $amount
        eventRound: $eventRound
        eventType: $eventType
      ) {
        ${BET}
      }
    }
  `,

  addPendingDeposit: gql`
    mutation(
      $txid: String!
      $eventAddress: String!
      $depositorAddress: String!
      $amount: String!
      $eventType: EventType!
    ) {
      addPendingDeposit(
        txid: $txid
        eventAddress: $eventAddress
        depositorAddress: $depositorAddress
        amount: $amount
        eventType: $eventType
      ) {
        ${DEPOSIT}
      }
    }
  `,

  addPendingResultSet: gql`
    mutation(
      $txid: String!
      $eventAddress: String!
      $centralizedOracleAddress: String!
      $resultIndex: Int!
      $amount: String!
      $eventRound: Int!
      $eventType: EventType!
    ) {
      addPendingResultSet(
        txid: $txid
        eventAddress: $eventAddress
        centralizedOracleAddress: $centralizedOracleAddress
        resultIndex: $resultIndex
        amount: $amount
        eventRound: $eventRound
        eventType: $eventType
      ) {
        ${RESULT_SET}
      }
    }
  `,

  addPendingWithdraw: gql`
    mutation(
      $txid: String!
      $eventAddress: String!
      $winnerAddress: String!
      $winningAmount: String!
      $creatorReturnAmount: String!
      $eventType: EventType!
    ) {
      addPendingWithdraw(
        txid: $txid
        eventAddress: $eventAddress
        winnerAddress: $winnerAddress
        winningAmount: $winningAmount
        creatorReturnAmount: $creatorReturnAmount
        eventType: $eventType
      ) {
        ${WITHDRAW}
      }
    }
  `,
};

class GraphMutation {
  constructor(client, mutationName, args) {
    this.client = client;
    this.mutationName = mutationName;
    this.mutation = MUTATIONS[mutationName];
    this.args = args;
  }

  execute = async () =>
    this.client.mutate({
      mutation: this.mutation,
      variables: this.args,
      fetchPolicy: 'no-cache',
    });
}

/**
 * Creates a new pending Event.
 * @param {ApolloClient} client Apollo Client instance.
 * @param {object} args Arguments for the mutation.
 * @return {object} Mutation result.
 */
export const addPendingEvent = async (client, args) => {
  const res = await new GraphMutation(client, MUTA_ADD_PENDING_EVENT, args)
    .execute();
  const { data: { addPendingEvent: event } } = res;
  return new Event(event);
};

/**
 * Creates a new pending Bet.
 * @param {ApolloClient} client Apollo Client instance.
 * @param {object} args Arguments for the mutation.
 * @return {object} Mutation result.
 */
export const addPendingBet = async (client, args) => {
  const res = await new GraphMutation(client, MUTA_ADD_PENDING_BET, args)
    .execute();
  const { data: { addPendingBet: bet } } = res;
  return new Bet(bet);
};

/**
 * Creates a new pending deposit.
 * @param {ApolloClient} client Apollo Client instance.
 * @param {object} args Arguments for the mutation.
 * @return {object} Mutation result.
 */
export const addPendingDeposit = async (client, args) => {
  const res = await new GraphMutation(client, MUTA_ADD_PENDING_DEPOSIT, args)
    .execute();
  const { data: { addPendingDeposit: deposit } } = res;
  return new Deposit(deposit);
};

/**
 * Creates a new pending Result Set.
 * @param {ApolloClient} client Apollo Client instance.
 * @param {object} args Arguments for the mutation.
 * @return {object} Mutation result.
 */
export const addPendingResultSet = async (client, args) => {
  const res = await new GraphMutation(client, MUTA_ADD_PENDING_RESULT_SET, args)
    .execute();
  const { data: { addPendingResultSet: resultSet } } = res;
  return new ResultSet(resultSet);
};

/**
 * Creates a new pending Withdraw.
 * @param {ApolloClient} client Apollo Client instance.
 * @param {object} args Arguments for the mutation.
 * @return {object} Mutation result.
 */
export const addPendingWithdraw = async (client, args) => {
  const res = await new GraphMutation(client, MUTA_ADD_PENDING_WITHDRAW, args)
    .execute();
  const { data: { addPendingWithdraw: withdraw } } = res;
  return new Withdraw(withdraw);
};

module.exports = {
  isProduction: () => process.env.NODE_ENV === 'production',
  intervals: { // in MS
    syncInfo: 5000,
    tooltipDelay: 300,
    snackbarLong: 5000,
    snackbarShort: 2000,
  },
  maxTransactionFee: 0.1,
  faqUrls: {
    'en-US': 'https://www.bodhi.network/faq',
    'zh-Hans-CN': 'https://cn.bodhi.network/faq',
  },
  urls: {
    nakaWalletWebStore: 'https://chrome.google.com/webstore/detail/NakaWallet/hdmjdgjbehedbnjmljikggbmmbnbmlnd',
    nakaWalletChrome: 'https://chrome.google.com/webstore/detail/naka-wallet/leopeeejkinfegnjkhpmpkaddnicjlll',
    nakaWalletAppStore: 'https://itunes.apple.com/us/app/naka-wallet/id1448562757',
    nakaWalletAppStoreChina: 'https://testflight.apple.com/join/MH8gFxwS',
    nakaWalletPlayStore: 'https://play.google.com/store/apps/details?id=com.nakachain.wallet',
    nakaWalletPlayStoreChina: 'https://nakachain.org/app-release_1.7.1_31.apk',
  },
  debug: {
    // Set to false if in test environment and Insight API is down
    // and loading screen is blocking the view.
    showAppLoad: false,
  },
  chainId: {
    mainnet: '2019',
    testnet: '2018',
  },
  network: {
    mainnet: 'mainnet',
    testnet: 'testnet',
  },
  storageKey: {
    locale: 'locale',
    tutorialDisplayed: 'tutorialDisplayed',
    favorites: 'favorites',
    appType: 'appType',
  },
  // The minimum contract versions that can be used.
  minContractVer: {
    mrEvent: 5,
    abEvent: 0,
    rewardEvent: 0,
  },
};

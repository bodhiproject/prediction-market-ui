/* eslint-disable */
/**
 * Use the contract version number as the object key.
 * IMPORTANT: The deployBlocks must be a block in the future that has not already
 * been synced.
 */
module.exports = {
  1: {
    mainnet: '0x9c19E9ee19dfe53E28b962507C2b741E86cA7752',
    mainnetDeployBlock: 6780194,
    testnet: '0xb017D0559094D17355Bb9522912cD140d7F59f06',
    testnetDeployBlock: 6655394,
    abi: [{"inputs": [{"name": "configManager","type": "address"}],"payable": false,"stateMutability": "nonpayable","type": "constructor"},{"anonymous": false,"inputs": [{"indexed": true,"name": "eventAddress","type": "address"},{"indexed": true,"name": "ownerAddress","type": "address"}],"name": "RewardEventCreated","type": "event"},{"constant": false,"inputs": [{"name": "from","type": "address"},{"name": "value","type": "uint256"},{"name": "data","type": "bytes"}],"name": "tokenFallback","outputs": [],"payable": false,"stateMutability": "nonpayable","type": "function"},{"constant": true,"inputs": [],"name": "version","outputs": [{"name": "","type": "uint16"}],"payable": false,"stateMutability": "pure","type": "function"}],
  },
  0: {
    mainnet: '',
    mainnetDeployBlock: 0,
    testnet: '0xE77c261065f491578e01eb087E74AE71095f0df5',
    testnetDeployBlock: 6267958,
    abi: [{"inputs":[{"name":"configManager","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"eventAddress","type":"address"},{"indexed":true,"name":"ownerAddress","type":"address"}],"name":"RewardEventCreated","type":"event"},{"constant":false,"inputs":[{"name":"from","type":"address"},{"name":"value","type":"uint256"},{"name":"data","type":"bytes"}],"name":"tokenFallback","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"uint16"}],"payable":false,"stateMutability":"pure","type":"function"}],
  },
};
/* eslint-enable */

import { sortBy, map } from 'lodash';
import { EVENT_TYPE } from 'constants';
import TokenExchangeMeta from './contracts/token-exchange';
import NakaBodhiTokenMeta from './contracts/naka-bodhi-token';
import ABEventFactoryMeta from './contracts/ab-event-factory';
import MREventFactoryMeta from './contracts/mr-event-factory';
import RewardEventFactoryMeta from './contracts/reward-event-factory';
import ABEventMeta from './contracts/ab-event';
import MREventMeta from './contracts/mr-event';
import RewardEventMeta from './contracts/reward-event';

/**
 * Gets the TokenExchange contract metadata.
 * @return {object} TokenExchange contract metadata.
 */
export const TokenExchange = () => TokenExchangeMeta;

/**
 * Gets the NakaBodhiToken contract metadata.
 * @return {object} NakaBodhiToken contract metadata.
 */
export const NakaBodhiToken = () => NakaBodhiTokenMeta;

/**
 * Gets the latest EventFactory contract metadata.
 * @param {string} eventType type of the Event Factory.
 * @return {object} Latest EventFactory contract metadata.
 */
export const EventFactory = (eventType) => {
  let EventFactoryMeta;
  switch (eventType) {
    case EVENT_TYPE.AB_EVENT: {
      EventFactoryMeta = ABEventFactoryMeta;
      break;
    }
    case EVENT_TYPE.MR_EVENT: {
      EventFactoryMeta = MREventFactoryMeta;
      break;
    }
    case EVENT_TYPE.REWARD_EVENT: {
      EventFactoryMeta = RewardEventFactoryMeta;
      break;
    }
    default: {
      throw Error('Invalid event type');
    }
  }
  let keys = Object.keys(EventFactoryMeta);
  keys = sortBy(map(keys, key => Number(key)));
  const latestVersion = keys[keys.length - 1];
  return EventFactoryMeta[`${latestVersion}`];
};

/**
 * Gets the Event contract metadata.
 * @param {string} eventType type of the Event contract.
 * @param {number} version Version of the contract.
 * @return {object} ABEvent contract metadata.
 */
export const getEventMeta = (eventType, version) => {
  switch (eventType) {
    case EVENT_TYPE.AB_EVENT: {
      return ABEventMeta[`${version}`];
    }
    case EVENT_TYPE.MR_EVENT: {
      return MREventMeta[`${version}`];
    }
    case EVENT_TYPE.REWARD_EVENT: {
      return RewardEventMeta[`${version}`];
    }
    default: {
      throw Error('Invalid event type');
    }
  }
};

#!/bin/bash
# Checkout to the tag.
# Runs the npm script to build/copy.

# any future command that fails will exit the script
set -e

# checkout to tag
cd prediction-market-ui
git fetch
git checkout -- .
git checkout $CI_COMMIT_TAG
git branch

# source the nvm file
source /home/ubuntu/.nvm/nvm.sh

# update deps
npm install

# deploy to mainnet
npm run $NPM_SCRIPT

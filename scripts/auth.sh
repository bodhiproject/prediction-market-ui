#!/bin/bash
# SSH into server and calls the deploy script.

# any future command that fails will exit the script
set -e

# Add private key
eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# disable host key checking
./scripts/disable-host-key-check.sh

# SSH and deploy
echo "Deploying to $SERVER_IP"
ssh ubuntu@$SERVER_IP "env CI_COMMIT_TAG=$CI_COMMIT_TAG NPM_SCRIPT=$NPM_SCRIPT bash" < $DEPLOY_SCRIPT_PATH

#!/bin/sh
# Builds the testnet UI.
# Meant to be run from the root folder.

# Set environment
export NETWORK=testnet
export API_HOSTNAME=testapi.puti.io
export SSL=true

echo "Building Testnet UI..."
node scripts/build.js

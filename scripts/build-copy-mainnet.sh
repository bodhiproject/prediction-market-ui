#!/bin/sh
# Builds and deploys the mainnet UI.
# Meant to be run from the root folder.

DEPLOY_PATH=/var/www/bodhi-pm-mainnet

# Set environment
export NETWORK=mainnet
export API_HOSTNAME=api.puti.io
export SSL=true

echo "Building Mainnet UI..."
node scripts/build.js

echo "Creating output path..."
sudo mkdir -p $DEPLOY_PATH

echo "Cleaning up old files..."
sudo rm -rf "$DEPLOY_PATH/*"

echo "Copying new website to $DEPLOY_PATH..."
sudo cp -a build/. $DEPLOY_PATH

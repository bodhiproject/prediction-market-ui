#!/bin/sh
# Builds and deploys the testnet UI.
# Meant to be run from the root folder.

DEPLOY_PATH=/var/www/bodhi-pm-testnet

# Set environment
export NETWORK=testnet
export API_HOSTNAME=testapi.puti.io
export SSL=true

echo "Building Testnet UI..."
node scripts/build.js

echo "Creating output path..."
sudo mkdir -p $DEPLOY_PATH

echo "Cleaning up old files..."
sudo rm -rf "$DEPLOY_PATH/*"

echo "Copying new website to $DEPLOY_PATH..."
sudo cp -a build/. $DEPLOY_PATH

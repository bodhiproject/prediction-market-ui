#!/bin/sh
# Builds the mainnet UI.
# Meant to be run from the root folder.

# Set environment
export NETWORK=mainnet
export API_HOSTNAME=api.puti.io
export SSL=true

echo "Building Mainnet UI..."
node scripts/build.js

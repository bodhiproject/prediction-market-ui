# Changelog

## Current

## 3.3.7

- [#71] - Fix withdraw object for mobile flow

## 3.3.6

- [#70] - Fix Slider crash

## 3.3.5

- [#58] - Update package.json
- [#60] - Simplify CI/CD stages

## 3.3.4

- [#57] - Fix order of updating pending txs for mobile flow

## 3.3.3

- [#56] - Add pmTx to transactions for mobile flow

## 3.3.2

- [#54] - Fix stages for Gitlab deployments

## 3.3.1

- [#51] - Fix missing postMessage call for mobile callback

## 3.3.0

- [#49] - Implement mobile callback flow
- [#52] - CD automated deployments

## 3.1.0

- [#32] - Update translation
- [#33] - Fix create event tx row has 0 amount for classic app
- [#34] - Translate logo
- [#38] - Hide reward
- [#39] - Unhide reward
- [#40] - Add query transaction with eventType
- [#41] - Make all type events in dashboard and give different event detail themes
- [#42] - Query success event only
- [#43] - Update ABEvent and RewardEvent metadata
- [#45] - Add close button for tutorial
- [#46] - Hide tutorial when go from event to prediction
- [#47] - Change default create event type
- [#50] - Update localization

## 3.0.3

- [#44] - Update build and deploy scripts

## 3.0.2

- [#37] - Add missing contract metadata for mainnet

## 3.0.1

- [#36] - Change blockNumber to blockNum for tx receipt schema

## 3.0.0

- [#3] - Create event page with winning chances
- [#4] - Update contracts
- [#5] - Update event card with new maxbets and odds
- [#6] - Update withdraw calculations
- [#7] - Add max-bet validation before bet
- [#8] - Rename multiple-result-event to ab-event
- [#9] - Update contracts
- [#10] - Add deposit tx in history
- [#11] - Add deposit field action and related methods
- [#12] - Fix withdraw page
- [#13] - Fix crash when bet over max bet
- [#14] - Only show maxbet text when it's betting status
- [#15] - Show house funding in withdraw page for creator
- [#16] - Add unpaidBet row when creator withdraw, adapt to new contract withdraw event
- [#17] - Hide odds smaller than 1 option
- [#18] - Change API urls and deploy paths
- [#19] - Fix local development port
- [#20] - Update contract
- [#21] - Merge Classic and House logic feature
- [#22] - Fix merge create event page
- [#24] - Set correct query versions based on appType
- [#25] - Add dropdown icon next to appType
- [#26] - Persist appType between sessions
- [#28] - Remove tailing 0s when auto fix number
- [#29] - Merge reward
- [#30] - Dynamically change theme based on app type, and settle down colors for house and reward
- [#35] - Make deposit option disabled outside of prebetting and betting

## 2.6.0

- [#1] - Update master with bodhi-ui
- [#2] - Automatic tagging CI/CD

## 2.5.0

- [#1549] - Add Pull Request template and add CHANGELOG.md
- [#1551] - Add Address Name Service

## 2.3.0

- [#1546] - Removed calculateWinnings calls and replace with query to backend
- [#1547] - Added leaderboard for return ratio

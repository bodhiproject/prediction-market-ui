# Prediction Market UI

## Requirements

- [Node](https://nodejs.org/en/) version greater than 8.6.0
- [Yarn](https://yarnpkg.com/lang/en/) or [npm](https://www.npmjs.com/) version greater than 6.0.0

## Install

```bash
$ git clone https://gitlab.com/bodhiproject/prediction-market-ui.git
$ cd prediction-market-ui
$ yarn
$ yarn upgrade # this is important
$ npm install
```

## Dev Environment

To run the development server, run the corresponding run script and the API will point to the remote server with the correct port. After compilation, it will show success commands & automatically redirect to the browser. Any code changes will be observed and will hot reload.

```bash
# Mainnet
$ cd prediction-market-ui
$ yarn start:mainnet

# Testnet
$ cd prediction-market-ui
$ yarn start:testnet

# Localhost server
$ cd prediction-market-ui
$ yarn start:local
```

## Tags

### Automated Tagging

Gitlab can create a new tag for you when a branch is merged to `master`. The tag will be pulled from the `package.json version`.

1. Login to the Gitlab repo
2. After merging a branch to `master`, it will trigger a new pipeline
3. Click on `CI/CD > Pipelines`
4. Find the pipeline of the branch you merged then click on the `Manual Job` button on the right
5. Select `tag:master`
6. A new tag will be created!

## Deployment

### Deploy Mainnet via Gitlab (Recommended)

When a new [tag](#tags) is created, you can trigger automated deploy to the mainnet from Gitlab.

1. After a tag is pushed, login to the Gitlab repo
2. Click on `CI/CD > Pipelines`
3. Find the pipeline of the tag and click on the `Manual Job` button on the right
4. Select `deploy:mainnet`
5. Gitlab will SSH and deploy the tag to mainnet

### Deploy Testnet via Gitlab (Recommended)

When a new [tag](#tags) is created, you can trigger automated deploy to the testnet from Gitlab.

1. After a tag is pushed, login to the Gitlab repo
2. Click on `CI/CD > Pipelines`
3. Find the pipeline of the tag and click on the `Manual Job` button on the right
4. Select `deploy:testnet`
5. Gitlab will SSH and deploy the tag to testnet

### Manual Deployment

This will create an optimized built version and deploy it to the `/var/www/` where Nginx will handle the routing. To deploy on the server:

1. SSH into server
2. cd `prediction-market-ui`
3. `npm run deploy:[mainnet|testnet]`

## Standards

### Javascript Standard

[![Airbnb Javascript Style Guide](https://camo.githubusercontent.com/546205bd8f3e039eb83c8f7f8a887238d25532d5/68747470733a2f2f7261772e6769746861636b2e636f6d2f746f6d656b77692f6a6176617363726970742f393566626638622f6261646765732f6269672e737667)](https://github.com/airbnb/javascript)

### Linting

```bash
$ npm run lint:fix    // get sweet link checking and fixing
$ npm run lint        // to see whats wrong
```

## Localization
`react-intl` is used for localization.

### Using FormattedMessage
- Try to use FormattedMessage whenever possible.
- `id` should match the id in the JSON file with all the strings.
- Put the default text inside `defaultMessage`.
- Dynamic variables can be declared in the `values` property.
```js
<FormattedMessage
  id='app.greeting'
  description='Greeting to welcome the user to the app'
  defaultMessage='Hello, {name}!'
  values={{
    name: 'Eric'
  }}
/>
```

### Using formatMessage
- For use with inline strings like string templates.
- Define messages at the top of the file using `defineMessages`.
```js
const messages = defineMessages({
  greeting: {
    id: 'app.greeting',
    defaultMessage: 'Hello, {name}!',
  },
});

const localizedMessage = this.props.intl.formatMessage(messages.greeting, { { name: 'Eric' }});
// localizedMessage = 'Hello, Eric!'
```

### Run Language Script
1. Run `npm run build:langs`
2. Update the newly translated strings in the corresponding language file. The language file is in `./src/languageProvider/locales`.

**[LGPL-3.0 License](https://github.com/bodhiproject/bodhi-ui/blob/master/LICENSE)**
